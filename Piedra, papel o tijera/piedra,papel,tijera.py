
#coding:utf8
from random import randint
numero=randint(1, 3)
solucion = input("piedra, papel o tijeras:")

"""numero 1 = piedra"""
"""numero 2 = papel"""
"""numero 3 = tijeras"""

#PIEDRA
if((solucion == "piedra") and (numero == 1)):
	print("Empate")
if((solucion == "piedra") and (numero == 2)):
	print("Has perdido")
if((solucion == "piedra") and (numero == 3)):
	print("Has ganado")

#PAPEL
if((solucion == "papel") and (numero == 2)):
	print("Empate")
if((solucion == "papel") and (numero == 3)):
	print("Has perdido")
if((solucion == "papel") and (numero == 1)):
	print("Has ganado")
		
#TIJERAS
if((solucion == "tijeras") and (numero == 3)):
	print("Empate")
if((solucion == "tijeras") and (numero == 1)):
	print("Has perdido")
if((solucion == "tijeras") and (numero == 2)):
	print("Has ganado")
