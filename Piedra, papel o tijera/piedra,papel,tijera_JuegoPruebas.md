| JUGADOR | MAQUINA | RESULTADO |
| ------- | ------- | --------- |
| Piedra | Piedra | Empate |
| Piedra | Papel | Has perdido |
| Piedra | Tijeras | Has ganado |
| Papel| Piedra | Has ganado |
| Papel | Papel | Empate |
| Papel | Tijeras | Has perdido |
| Tijeras | Piedra | Has perdido |
| Tijeras | Papel | Has ganado |
| Tijeras | Tijeras | Empate |