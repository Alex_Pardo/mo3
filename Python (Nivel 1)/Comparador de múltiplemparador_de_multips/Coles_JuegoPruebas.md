```
Escriba un programa que pida dos números enteros positivos y que escriba si el mayor es múltiplo del menor.
COMPARADOR DE MÚLTIPLOS:
Escriba un número: 48
Escriba otro número: 6
48 es múltiplo de 6.
```

Juego de pruebas:

| Primer nuemro | Segundo numero | Pruebas |
| ------------- | -------------- | ------- |
| 8  | 2  | 6 es multiple de 2 |
| 2  | 8  | 6 es multiple de 2 |
| 8  | 8  | Son iguales |
| 5  | 2  | 5 no es multiple de 2 |
| 2  | 5  | 5 no es multiple de 2 |
| 5  | 5  | Son iguales |