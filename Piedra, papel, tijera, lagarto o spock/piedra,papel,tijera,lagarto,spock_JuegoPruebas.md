| JUGADOR | MAQUINA | RESULTADO |
| ------- | ------- | --------- |
| Piedra | Piedra | Piedra contra piedra, ha sido empate |
| Piedra | Papel | El papel envuelve a la piedra, has perdido |
| Piedra | Tijeras | La piedra aplasta tijeras, has ganado |
| Piedra | Lagarto | Piedra aplasta al lagarto, has ganado |
| Piedra | Spock | Spock vaporiza a la piedra, has perdido |

| JUGADOR | MAQUINA | RESULTADO |
| ------- | ------- | --------- |
| Papel| Piedra | El papel envuelve a la piedra, has ganado |
| Papel | Papel | Papel contra papel, ha sido empate |
| Papel | Tijeras | Las tijeras cortan el papel, has perdido |
| Papel | Lagarto | El lagarto se come el papel, has perdido |
| Papel | Spock | EL papel refuta al Spock, has ganado |

| JUGADOR | MAQUINA | RESULTADO |
| ------- | ------- | --------- |
| Tijeras | Piedra | La piedra aplasta tijeras, has perdido |
| Tijeras | Papel | Las tijeras cortan el papel, has ganado |
| Tijeras | Tijeras | Tijeras contra tijeras, ha sido empate |
| Tijeras | Papel | Las tijeras decapitan al lagarto, has ganado |
| Tijeras | Tijeras | El Spock destroza las tijeras, has perdido |

| JUGADOR | MAQUINA | RESULTADO |
| ------- | ------- | --------- |
| Lagarto | Piedra | La piedra aplasta al lagarto, has perdido |
| Lagarto | Papel | El lagarto se come al papel, has perdido |
| Lagarto | Tijeras | Las tijeras decapitan al lagarto, has perdido |
| Lagarto | Lagarto | Lagarto contra lagarto, ha sido empate |
| Lagarto | Spock | El lagarto envenena al Spock, has ganado |

| JUGADOR | MAQUINA | RESULTADO |
| ------- | ------- | --------- |
| Spock | Piedra | Spock vaporiza a la piedra, has ganado |
| Spock | Papel | EL papel refuta al Spock, has perdido |
| Spock | Tijeras | El Spock destroza las tijeras, has ganado |
| Spock | Lagarto | El lagarto envenena al Spock, has perdido |
| Spock | Spock | Spock contra Spock, ha sido empate |