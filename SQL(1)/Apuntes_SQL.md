## APUNTES:

BD Relacionales      BD Domésticas:     BD Empresa:
Teoria conjuntos     M.S access         oracle (P)     
                     L.O base           Informix (P)

Maria DB → Menor volumen de datos
PostGre SQL → Mayor volumen de datos

Campos = Columnas
Registros = FilasR
Base de datos = conjunto de tablas
MS SQL SERVER

sudo -i: Si tienes permisos con este usuario, puedes ser root.
A.D Active Directory
Perfiles móviles: Son aquellos ordenadores que están configurados para almacenar sus datos en un servidor (para empresas). 

Para finalizar el postgres, hay que hacer (;) para que sepa que ya hemos acabado, sino interpreta que queremos seguir escribiendo.

## POSTGRE:
```
# systemctl status postgres
# systemctl start postgresql
# su - postgres
# cp /home/users/inf/inf/ins……./Descargas /tmp 
# create database training → crear base de datos
# ; → para finalizar
# \l → comprobar que se a creado
# \c training → para conectar con la base de datos    
# \q → para salir de la base de datos
# \d → muestra todas la tablas de la BBDD
# select from clients → tabla clientes
NO configurar que se inicie automáticamente (apartado 7)
```

## INSTALACIÓN:
```
# dnf -y install postgresql-server
# rpm -qa | grep postgres
# postgresql-setup initdb
# systemctl status postgresql
# systemctl start postgresql
# systemctl status postgresql
# passwd postgres
# id postgres
# exit (root)
```

## COMANDAS:
```
1.  Ctrl + R: para ir buscando comandas parecidas en terminal.
2.  dnf install libreoffice-base
    Nic Bonom b/seb
3.  sudo -i: Si tienes permisos con este usuario, puedes ser root.
```

## DEFINICION DE COLLATE Y CTYPE(SQL):
```
El soporte de conjuntos de caracteres en PostgreSQL le permite almacenar texto en una variedad de conjuntos de caracteres (codificaciones), 
incluidos los conjuntos de caracteres de un solo byte y los conjuntos de caracteres de múltiples bytes, UTF-8, y código interno de Mule. 
Se pueden usar todos los conjuntos de caracteres compatibles, pero algunos no son compatibles con el uso dentro del servidor. El conjunto de 
caracteres predeterminado se selecciona al inicializarsu clúster de base de datos PostgreSQL usando initdb. Puede anularse al crear una base 
de datos, por lo que puede tener varias bases de datos, cada una con un conjunto de caracteres diferente. Una restricción importante es que 
el conjunto de caracteres de cada base de datos debe ser compatible con la configuración regional LC_CTYPE (clasificación de caracteres) y 
LC_COLLATE (orden de clasificación de cadenas) de la base de datos. Para la configuración regional C o POSIX, se permite cualquier conjunto
de caracteres, pero para otras configuraciones regionales solo hay un conjunto de caracteres que funcionará correctamente.

El propósito principal de collate y de ctype es controlar el orden de clasificación en una base de datos para servidores. 
```

## POSTGRES COMANDAS:
```
SELECT  *                   SELECT  *                   SELECT  *
FROM	oficinas            FROM    oficinas            FROM	oficinas
WHERE 	(region='Est')      WHERE   (objetivo<600000)   WHERE   region='Este'
;                           ;                           AND     director IN(105,106)
                                                        ;

SELECT  *                   SELECT  *                   SELECT  *
FROM	oficinas            FROM    oficinas            FROM    oficinas
WHERE 	(region='Oeste')    WHERE   (ventas>300000)     WHERE 	region='Oeste'
;                           ;                           AND     ventas>objetivo
                                                        ;

SELECT  *                   SELECT  *                   SELECT  *
FROM	oficinas            FROM    oficinas            FROM	oficinas
WHERE 	(ventas>objetivo)   WHERE   ciudad='Denver'     WHERE 	region='Oeste'
;                           OR      ciudad='Chicago'    AND     ventas<objetivo
                            ;                           ;                           

SELECT  *                   SELECT  *
FROM	oficinas            FROM    oficinas
WHERE 	(director=108)      WHERE   oficina IN(11,12,22)
;                           ;

SELECT  *                   SELECT  *
FROM	oficinas            FROM    oficinas
WHERE 	(objetivo>500000)   WHERE   region='Este'
;                           AND     director=105
                            ;
# Cuando tienes varios datos que filtrar, como cuidades, se puede poner poner IN
(los datos con comas), asi de esta manera no hay poner un monton de ORs.
EX:
SELECT  *
FROM    oficinas
WHERE   ciudad IN('Denver','Chicago','Los Angeles')
;
```
OTROS EJEMPLOS:
```
SELECT      id producto, COUNT(*)
FROM        productos
GROUP BY    id producto
HAVING      COUNT(*) >=2
;

SELECT *
FROM   producto
WHERE  id_producto SIMILAR TO '_[0-9]%'
;

SELECT *
FROM   productos
WHERE  id_producto SIMILAR TO '[0-9-A-Z-a-z]%'
;

SELECT *
FROM   productos
WHERE  id_producto SIMILAR TO '[a-zA-Z]%'
;

SELECT *
FROM   productos
WHERE  id_producto SIMILAR TO '[aA-zZ]%'
;

SELECT *
FROM   clientes
WHERE  DNI SIMILAR TO '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][A-Z]'
;

SELECT      descripcion, COUNT(*)
FROM        productos
GROUP BY    descripcion
HAVING      COUNT(*) >=2
;
```

## EJEMPLOS SQL (DEBERES):  
```
EJEMPLOS SQL (DEBERES):   
LO QUE HACE ESTE SQL ES SELECIONAR LAS FILAS NUM_CLIE, EMPRESA, REP_CLIE Y LIMITE_CREDITO DE LA FILA CLIENTES. DESPUÉS DE ESTO, LAS AGRUPA
Y LAS FILTRA POR EL LIMITE DE CREDITO DE CADA CLIENTE,CON SU EMPRESA Y SU NUMERO DE EMPLEADO.

SELECT      num_clie, empresa, rep_clie, limite_credito
FROM        clientes 
GROUP BY    num_clie, empresa, rep_clie, limite_credito  
HAVING      limite_credito > 10000.00  
ORDER BY    num_clie ; 

LO QUE HACE ESTE SQL ES SELECCIONAR LA FILA OFICINA, DESPUÉS SELECCIONA LA REGION DE OESTE. A CONTINUACIÓN, FILTRARA LAS FILAS LAS CUALES EL 
OBJETIVO SUPERE A LAS VENTAS, Y CUANDO LAS VENTAS SUPERAN LOS 100.000.

SELECT      *
FROM        oficinas
WHERE       region='Oeste'
AND         ventas<objetivo
AND         (ventas<100.000)
```

## NORMAS POSTGRE
```
WILDCARDS (LINUX)
*A = Que contenga una A

POSTGRES
%A = Que acabe con A
%A% = Que contenga una A 
A% = Que empienza por A
```

## SQL MÁS COMPLICADO
```
SELECT	   num_empl AS "numero_empleado",
           nombre,(ventas/cuota) AS "ratio"
FROM       repventas
WHERE      (ventas/cuota) <= 1
ORDER BY   (ventas/cuota) ASC;

SELECT	   num_empl AS numero_empleado,
           nombre,(ventas/cuota) AS ratio
FROM       repventas
WHERE      (ventas/cuota) <= 1
ORDER BY   (ventas/cuota) ASC;

SELECT      descripcion,
            existencias * precio AS valor
FROM        productos
ORDER BY    (existencias * precio) DESC
LIMIT       10;
```

## EJEMPLOS SUBSTRING
```
SELECT SUBSTRING(descripcion, 2, 5) AS ExtractString
FROM precio;

SELECT empresa || '-' ||
         SUBSTRING(id_producto from 2 for 4) || '/' ||
         SUBSTRING(id_producto from 5 for 3)
         AS "Hola"
FROM     clientes;


SELECT empresa || '-' ||
         SUBSTR(id_producto,2,3) || '/' ||
         SUBSTR(id_producto,5,2)
         AS "Clau primària"
FROM   clientes;
```

## UN EJEMPLO DE CADA UNO
```
**IN**
SELECT *
FROM   clientes
WHERE  empresa IN('Ian','JCP');

**NOT IN**
SELECT *
FROM   clientes
WHERE  num_clie || limite_credito NOT IN (SELECT DISTINCT cliente || credito FROM empresa);

**LIKE**
SELECT *
FROM   clientes
WHERE  empresa LIKE 'a%';

**ILIKE**
SELECT *
FROM   clientes
WHERE  empresa LIKE 'a%';

**SIMILAR TO**
SELECT empresa
FROM   clientes
WHERE  empresa SIMILAR TO '_[a-i]%';

**DISTRICT**
SELECT DISTINCT id_fab
FROM   productos;

**AS**
SELECT num_clie         AS "Número de cliente",
       empresa          AS "Empresa",
       rep clie         AS "Rep Clie",
       limite_credito   AS "Limite de credito",
FROM   clientes;

**IS NULL**
SELECT      num_clie, empresa, limite_credito
FROM        clientes
WHERE       cuota IS NULL;

**IS NOT NULL**
SELECT   DISTINCT cuota
FROM     oficina_rep
WHERE    oficina_rep IS NOT NULL;

**ROUND**
SELECT      num_empl AS NUM_EMPLEADO,
            nombre,
            ROUND( (ventas/cuota) , 2 ) AS ratio
FROM        repventas
WHERE       (ventas/cuota) < 1
ORDER BY    (ventas/cuota) ASC;

**LIMIT**
SELECT      empresa
FROM        clientes
LIMIT 10;

**BETWEEN**
SELECT      descripcion,
FROM        precio
WHERE       descripcion BETWEEN value1 AND value2
```
## NOT IN
