#coding: utf-8
"""Menu para una calculadora
"""
from os import system
from time import sleep
import funcion_leer_numero

def restar (a,b):
    resultad=0
    resultado=a-b
    return resultado

def sumar (a,b):
    resultad=0
    resultado=a+b
    return resultado

def multiplicar (a,b):
    resultad=0
    resultado=a*b
    return resultado

def dividir (a,b):
    resultad=0
    resultado=a/b
    return resultado

def mostrar_menu ():
    print("0 -> salir")
    print("1 -> suma")
    print("2 -> resta")
    print("3 -> multiplicacion")
    print("4 -> division")



def menu():
    # Inicializaciones
    salir = "N"

    while ( salir=="N" ):
        mostrar_menu()
        # Activo indicador de salida si toca
        print("Elige una opción")
        opcion=funcion_leer_numero.leer_numero_entero("")

        if opcion>=0 and opcion<=4: # Condición de salida
            salir = "S"
        else:
            print ("La opcion no es correcta")
            sleep(1)
            system('clear')


    return opcion
    
########################################################################

if __name__== "__main__":

    salir = "N"


    while ( salir=="N" ):
        opcion_escogida=menu()

        if (opcion_escogida!=0):
            num1=int(input("Elige el primer numero: "))
            num2=int(input("Elige el segundo numero: "))


        if  (opcion_escogida==1):
            total=sumar(num1,num2)
            print ("El resultado de la suma es", total)
            sleep(2.5)
            system('clear')
            
        elif (opcion_escogida==2):
            total=restar(num1,num2)
            print ("El resultado de la resta es", total)
            sleep(2.5)
            system('clear')
            
        elif (opcion_escogida==3):
            total=multiplicar(num1,num2)
            print ("El resultado de la multiplicación es", total)
            sleep(2.5)
            system('clear')
            
        elif (opcion_escogida==4):
            total=dividir(num1,num2)
            print ("El resultado de la división es", total)
            sleep(2.5)
            system('clear')
            
        elif (opcion_escogida==0):
            print("Adios")
            sleep(1)
            system('clear')
            salir="S"
