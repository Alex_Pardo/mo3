#coding:utf8

"""Juego de piedra, papel, tijera, lagarto, spock.
"""

################################################################################
#									IMPORT 								       #
################################################################################


# Número random
from random import randint
# Para poder definir un intervalo de tiempo
from time import sleep
# Para poder limpiar entre menus
from os import system

################################################################################
#							  VARIABLES GLOBALES 							   #
################################################################################


################################################################################
#									NIVEL 5 								   #
################################################################################

def opcion_valida(opcion_usuario,opcion_maquina):

    # Comparamos la opcion del usuario con la opcion de la maquina
    # para saber quien ha ganado        # PIEDRA
    if((opcion_usuario == 1) and (leer_opcion_maquina == 1)):
    	print("Piedra contra piedra, ha sido empate")
    elif((opcion_usuario == 1) and (leer_opcion_maquina == 2)):
    	print("El papel envuelve a la piedra, has perdido")
    elif((opcion_usuario == 1) and (leer_opcion_maquina == 3)):
    	print("La piedra aplasta tijeras, has ganado")
    elif((opcion_usuario == 1) and (leer_opcion_maquina == 4)):
    	print("Piedra aplasta al lagarto, has ganado")
    elif((opcion_usuario == 1) and (leer_opcion_maquina == 5)):
    	print("Spock vaporiza a la piedra, has perdido")

    # Comparamos la opcion del usuario con la opcion de la maquina
    # para saber quien ha ganado        # PAPEL
    elif((opcion_usuario == 2) and (leer_opcion_maquina == 1)):
    	print("El papel envuelve a la piedra, has ganado")
    elif((opcion_usuario == 2) and (leer_opcion_maquina == 2)):
    	print("Papel contra papel, ha sido empate")
    elif((opcion_usuario == 2) and (leer_opcion_maquina == 3)):
    	print("Las tijeras cortan el papel, has perdido")
    elif((opcion_usuario == 2) and (leer_opcion_maquina == 4)):
    	print("El lagarto se come el papel, has perdido")
    elif((opcion_usuario == 2) and (leer_opcion_maquina == 5)):
    	print("EL papel refuta al Spock, has ganado")

    # Comparamos la opcion del usuario con la opcion de la maquina
    # para saber quien ha ganado        #TIJERAS
    elif((opcion_usuario == 3) and (leer_opcion_maquina == 1)):
    	print("La piedra aplasta tijeras, has perdido")
    elif((opcion_usuario == 3) and (leer_opcion_maquina == 2)):
    	print("Las tijeras cortan el papel, has ganado")
    elif((opcion_usuario == 3) and (leer_opcion_maquina == 3)):
    	print("Tijeras contra tijeras, ha sido empate")
    elif((opcion_usuario == 3) and (leer_opcion_maquina == 4)):
    	print("Las tijeras decapitan al lagarto, has ganado")
    elif((opcion_usuario == 3) and (leer_opcion_maquina == 5)):
    	print("El Spock destroza las tijeras, has perdido")

    # Comparamos la opcion del usuario con la opcion de la maquina
    # para saber quien ha ganado       #LAGARTO
    elif((opcion_usuario == 4) and (leer_opcion_maquina == 1)):
    	print("Lia piedra aplasta al lagarto, has perdido")
    elif((opcion_usuario == 4) and (leer_opcion_maquina == 2)):
    	print("El lagarto se come al papel, has perdido")
    elif((opcion_usuario == 4) and (leer_opcion_maquina == 3)):
    	print("Las tijeras decapitan al lagarto, has perdido")
    elif((opcion_usuario == 4) and (leer_opcion_maquina == 4)):
    	print("Lagarto contra lagarto, ha sido empate")
    elif((opcion_usuario == 4) and (leer_opcion_maquina == 5)):
    	print("El lagarto envenena al Spock, has ganado")

    # Comparamos la opcion del usuario con la opcion de la maquina
    # para saber quien ha ganado        #SPOCK
    elif((opcion_usuario == 5) and (leer_opcion_maquina == 1)):
    	print("Spock vaporiza a la piedra, has ganado")
    elif((opcion_usuario == 5) and (leer_opcion_maquina == 2)):
    	print("EL papel refuta al Spock, has perdido")
    elif((opcion_usuario == 5) and (leer_opcion_maquina == 3)):
    	print("El Spock destroza las tijeras, has ganado")
    elif((opcion_usuario == 5) and (leer_opcion_maquina == 4)):
    	print("El lagarto envenena al Spock, has perdido")
    elif((opcion_usuario == 5) and (leer_opcion_maquina == 5)):
    	print("Spock contra Spock, ha sido empate")

    # Cuando el usurio ponga un número que no sea correcto, le dara error mostrando un
	# texto en pantalla, y después le saldra de nuevo el texto de la variable opcion
    return opcion_valida

################################################################################
#									NIVEL 4 								   #
################################################################################

def leer_opcion_teclado ():
    # Lee un número entero del usuario
	# Tiene que ser correcto
	# No salimos del bucle hasta que lo sea

    salir = "N"

    # Al importar funcion_leer_numero tenemos que llamarlo
    while (salir == "N"):
        opcion = int(input("Indique con el número correspondiente la opcion que quiera seleccionar: "))

        # Limpiamos la pantalla para que no se llene con los menus. De esta manera,
		# cuando el usurio pase de menu o se equivoque y vuleva a salir el menu,
		# desaparecera el menu anterior.
        system("clear")

        if (opcion >= 1) and (opcion <= 5): # Condición de salida
            salir = "S"
        else:
            print ("La opcion introducida no es correcta, intentelo de nuevo")
            print ("")
            sleep(2)
            system("clear")

    # Cuando el usurio ponga un número que no sea correcto, le dara error mostrando un
	# texto en pantalla, y después le saldra de nuevo el texto de la variable opcion
    return opcion

################################################################################
#									NIVEL 3 								   #
################################################################################

# Muestra el menu del usuario donde mostramos las opciones
# que puede elegir
def mostrar_menu():
    print("MENU USUARIO")
    print("1) Piedra")
    print("2) Papel")
    print("3) tijeras")
    print("4) Lagarto")
    print("5) Spock")

def	leer_opcion():
    opcion_usuario = leer_opcion_teclado ()

################################################################################
#									NIVEL 2 								   #
################################################################################

# La opcion que va a elegir el usuario
def leer_opcion_usuario():
	mostrar_menu() # Eresultado = opcion_valida(opcion)l menu que va a ver el usuario
	opcion = leer_opcion() # Va a leer la opcion del usuario

# La opcion aleatoria de la maquina
def leer_opcion_maquina(): # Va a leer la opcion que ha elegido aleatoriamente la maquina
    return randint(1,5) # Si algo esta incorrecto, volvera a la variable return

################################################################################
#								NIVEL 1 (MAIN)								   #
################################################################################
if __name__ == "__main__":

	# Obtenemo la opcion del usuario
	opcion_usuario=leer_opcion_usuario()

	# Obtenemo la opcion de la maquina
	opcion_maquina=leer_opcion_maquina()

    opcion_valida(opcion_usuario,opcion_maquina)